
name := """play-java-starter-example"""

version := "1.0-SNAPSHOT"

//import com.github.play2war.plugin._

//Play2WarPlugin.play2WarSettings

//Play2WarKeys.servletVersion := "3.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.8"

javacOptions ++= Seq(
  "-encoding", "UTF-8",
  "-parameters",
  "-Xlint:unchecked",
  "-Xlint:deprecation",
  "-Werror"
)

crossScalaVersions := Seq("2.11.12", "2.12.7")

libraryDependencies += guice
libraryDependencies += "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided"

// Test Database
libraryDependencies += "com.h2database" % "h2" % "1.4.197"

// Testing libraries for dealing with CompletionStage...
libraryDependencies += "org.assertj" % "assertj-core" % "3.11.1" % Test
libraryDependencies += "org.awaitility" % "awaitility" % "3.1.3" % Test
libraryDependencies += "org.webjars" % "bootstrap" % "3.3.1"
// https://mvnrepository.com/artifact/org.apache.tomcat.maven/tomcat7-maven-plugin
libraryDependencies += "org.apache.tomcat.maven" % "tomcat7-maven-plugin" % "2.2"
//libraryDependencies += "com.github.play2war" % "play2-war_2.9.1" % "0.8.2"


// Make verbose tests
testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))


//enablePlugins(TomcatPlugin)